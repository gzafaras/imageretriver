require 'phashion'
class PostsController < ApplicationController
  before_action :set_post, only: [:show, :edit, :update, :destroy]
  
  # GET /posts
  # GET /posts.json
  def index
    @posts = Post.all
  end

  # GET /posts/1
  # GET /posts/1.json
  def show
    
    @post = Post.find(params[:id])
    @similarpost = Array.new
    @posts = Post.all
    img = Phashion::Image.new @post.image.path(:medium)
    
    @posts.each do |p|
      tmpimg = Phashion::Image.new p.image.path(:medium)
      if(img.distance_from(tmpimg)< 100 and !img.duplicate?(tmpimg, :threshold => 0))
        @similarpost.push p
      end    
    end 
  end
  
  def search
    @posts = Post.all
    @post = Post.find((params[:id]))
    @pos_im = Array.new
    @newimage = Array.new
    sumfingerprint = 0
    #@post.positive_images.each do f
    imgparams = params['post']['positive_images']
    imgparams.each do |f|
      
      @pos_im.push Post.find(f.to_i)
    end
    img = Phashion::Image.new @post.image.path(:medium)
    @pos_im.each do |p|
      tmpimg = Phashion::Image.new p.image.path(:medium)
      sumfingerprint = sumfingerprint + tmpimg.fingerprint 
    end
      avfingerprint = 0.2 * sumfingerprint + 0.8 * img.fingerprint / imgparams.length + 1
      @posts.each do |p|
      tmp = Phashion::Image.new p.image.path(:medium)
      if(Phashion.hamming_distance(tmp.fingerprint,avfingerprint)< 29 and !tmp.duplicate? img )
        @newimage.push p
      end    
    end 
      #@pos_im.push tmp
    
    
  end


  # GET /posts/new
  def new
    @post = Post.new
  end

  # GET /posts/1/edit
  def edit
  end

  # POST /posts
  # POST /posts.json
  def create
    @post = Post.new(post_params)

    respond_to do |format|
      if @post.save
        format.html { redirect_to @post, notice: 'Post was successfully created.' }
        format.json { render action: 'show', status: :created, location: @post }
      else
        format.html { render action: 'new' }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /posts/1
  # PATCH/PUT /posts/1.json
  def update
    respond_to do |format|
      if @post.update(post_params)
        format.html { redirect_to @post, notice: 'Post was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /posts/1
  # DELETE /posts/1.json
  def destroy
    @post.destroy
    respond_to do |format|
      format.html { redirect_to posts_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_post
      @post = Post.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def post_params
      params.require(:post).permit(:name, :title, :content,:image)
    end
end
